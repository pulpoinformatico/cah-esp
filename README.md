# CAH-ESP
Script para generar un set del juego Cartas Contra la Humanidad

# Uso
Ejecuta el script en Scribus, selecciona el fichero .csv con las frases para cartas blancas, y después selecciona el fichero .csv con las frases para las cartas negras.

# Funcionalidades
- El script añade automáticamente "JUEGA X" cuando encuentra dos o más huecos para jugar carta blanca.
