#!/usr/bin/env python

# @name CAH-ESP
# @author ElderAegia
# @version 1.0
# @description This script creates a set of Cards Against Humanity from CSV files.
# http://cardsagainsthumanity.com/
#
# scribus script > 1.4 compatible
# Based on uno.py by Giorgos Logiotatidis and importcvs2table by Sebastian Stetter 
# Modified version of Lang HOANG's CAH script (hoanglang@gmail.com)
# Under the GPL Licence

# Usage :
# - launch script from scribus
# - Select the CSV file with white cards' text
# - Select the CSV file with black cards' text
#
# The default format is A4 - landscape.

from __future__ import division
import sys

try:
    # Please do not use 'from scribus import *' . If you must use a 'from import',
    # Do so _after_ the 'import scribus' and only import the names you need, such
    # as commonly used constants.
    import scribus
except ImportError,err:
    print "This Python script is written for the scribus scripting interface."
    print "It can only be run from within scribus."
    sys.exit(1)

import csv

# PAGE FORMAT
# 210 x 297 mm A4 size
WIDTH=210 # page width in mm
HEIGHT=297 # page height in mm
MARGINS=(10, 10, 10, 10) # margins (left, right, top, bottom) ?

# CARD SIZE (Standard card size 68 x 88 mm)
CARD_W=63 # card width
CARD_H=69 # card height

# Get the cvs data
def getCSVdata(delim, qc):
    """opens a csv file, reads it in and returns a 2 dimensional list with the data"""
    csvfile = scribus.fileDialog("Select file", "*.csv")
    if csvfile != "":
        try:
            reader = csv.reader(file(csvfile), delimiter=delim, quotechar=qc)
            datalist=[]

            for row in reader:
                datalist.append(row)
            return datalist

        except Exception,  e:
            scribus.messageBox("Error", "Could not open file %s"%e)
    else:
        sys.exit

# Compute the number of cards per line/row
def get_multipl(total, width, margin_left=0, margin_right=0):
    e=divmod(total-(margin_left+margin_right), width)
    return e[0]

# add the CAH logo
def addLogo(x, y, isBlack):
    #square side
    a=5
    line=0.01
    angle=15
    scribus.defineColor("LightGrey",0,0,0,128)
    scribus.defineColor("DarkGrey",0,0,0,192)

    # White cards colors
    squareColor1="Black"
    squareColor2="DarkGrey"
    squareColor3="LightGrey"
    lineColor1="White"
    lineColor2="White"
    lineColor3="White"
    titleColor="Black"

    # Black cards colors
    if isBlack==True:
        squareColor1="DarkGrey"
        squareColor2="LightGrey"
        squareColor3="White"
        lineColor1="DarkGrey"
        lineColor2="LightGrey"
        lineColor3="White"
        titleColor="White"

    square1=scribus.createRect(x, y+a/10, a, a)
    scribus.setLineWidth(1, square1)
    scribus.setFillColor(squareColor1, square1)
    scribus.setLineColor(lineColor1, square1)
    scribus.rotateObject(angle, square1)

    square2=scribus.createRect(x+a/2, y, a, a)
    scribus.setLineWidth(1, square2)
    scribus.setFillColor(squareColor2, square2)
    scribus.setLineColor(lineColor2, square2)

    square3=scribus.createRect(x+a, y, a, a)
    scribus.setLineWidth(1, square3)
    scribus.setFillColor(squareColor3, square3)
    scribus.setLineColor(lineColor3, square3)
    scribus.rotateObject(-angle, square3)

    title=scribus.createText(x+10.5,y+2.5, 46, 4)
    scribus.setFont("Arial Bold", title)
    scribus.setFontSize(9, title)
    scribus.insertText("Cartas Contra la Humanidad", 0, title)
    scribus.setTextColor(titleColor, title)

    return

# add DRAW / PICK text on black cards
def addCaption(W_position, H_position, number):
    # add PICK and DRAW texts
    spx=W_position+22
    spy=H_position+40

    # Main Captions
    pickText=scribus.createText(spx, spy, 26, 6)
    scribus.setFont("Arial Bold", pickText)
    scribus.setFontSize(16, pickText)
    scribus.insertText("JUEGA", 0, pickText)
    scribus.setTextColor("White", pickText)

    # The Circle
    pickCircle=scribus.createEllipse(spx+26, spy-2, 8, 8)
    scribus.setFillColor("White", pickCircle)
    scribus.setLineColor("White", pickCircle)

    # Number Inside Circle
    pickNumber=scribus.createText(spx+28.2, spy, 4, 6)
    scribus.setFont("Arial Bold", pickNumber)
    scribus.setFontSize(16, pickNumber)
    scribus.setTextColor("Black", pickNumber)
    scribus.insertText(str(number), 0, pickNumber)


# create a new card
# Order of the layers :
# 1-Box
# 2-TextBox
# 3-Logo
def createCell(text, roffset, coffset, card_width, card_height, margin_left, margin_top, isBlack):

    W_position = margin_left+(roffset-1)*card_width
    H_position = margin_top+(coffset-1)*card_height

    box=scribus.createRect(W_position, H_position, card_width, card_height)
    textBox=scribus.createText(W_position, H_position, card_width, card_height)

    #Default font
    scribus.setFont("Arial Bold", textBox)
    fontSize=16
    lineSpacing=18

    if len(text) > 20:
        fontSize=14
        lineSpacing=16

    scribus.setFontSize(fontSize, textBox)
    scribus.setLineSpacing(lineSpacing, textBox)
    scribus.setTextDistances(4, 4, 4, 4, textBox)
    scribus.insertText(text, 0, textBox)

    #add Logo
    addLogo(W_position+5,H_position+60, isBlack)

    #add caption text
    if isBlack:
        scribus.setFillColor("Black", box)
        scribus.setLineColor("White", box)
        scribus.setTextColor("White", textBox)
        numberCards = text.count("______")
        if numberCards >= 2:
            addCaption(W_position, H_position, numberCards)
    return

# Main
colstotal=get_multipl(WIDTH, CARD_W, MARGINS[0], MARGINS[1])
rowstotal=get_multipl(HEIGHT, CARD_H, MARGINS[2], MARGINS[3])
#print "n per rows: "+ str(colstotal) + ", n per cols: " + str(rowstotal)

# create a new document
t=scribus.newDocument((WIDTH,HEIGHT), MARGINS, scribus.PORTRAIT, 1, scribus.UNIT_MILLIMETERS, scribus.FACINGPAGES, scribus.FIRSTPAGERIGHT,1)
count_row=1
count_column=1
nol=0

# open CSV file
white_cards = getCSVdata(delim=';', qc='"')
isBlack=False

# Process all data
scribus.messagebarText("Processing "+str(nol)+" elements")
scribus.progressTotal(len(white_cards))
for line in white_cards:
    scribus.messagebarText("Processing "+str(nol)+" elements")
    cardtext = line[0].strip()

    createCell(cardtext, count_row, count_column, CARD_W, CARD_H, MARGINS[0], MARGINS[2], isBlack)
    nol+=1
    scribus.progressSet(nol)

    if nol >= len(white_cards):
        break # prevents adding a new page

    if count_row==colstotal and count_column==rowstotal:
        # starts new page
        scribus.newPage(-1)
        scribus.gotoPage(scribus.pageCount())
        count_row=1
        count_column=1
    elif count_row==colstotal:
        # starts new column
        count_row=1
        count_column+=1
    else:
        count_row+=1

# add blank cards to fill the rest of page
while count_column<=rowstotal:
    createCell("", count_row, count_column, CARD_W, CARD_H, MARGINS[0], MARGINS[2], isBlack)

    if count_row==colstotal:
        # starts new column
        count_row=1
        count_column+=1
    else:
        count_row+=1

scribus.messagebarText("Processed white cards.")


black_cards = getCSVdata(delim=';', qc='"')
isBlack=True
scribus.newPage(-1)
scribus.gotoPage(scribus.pageCount())
count_row=1
count_column=1
nol=0
# Process data
scribus.messagebarText("Processing "+str(nol)+" elements")
scribus.progressTotal(len(black_cards))

for line in black_cards:
    scribus.messagebarText("Processing "+str(nol)+" elements")
    cardtext = line[0].strip()

    createCell(cardtext, count_row, count_column, CARD_W, CARD_H, MARGINS[0], MARGINS[2], isBlack)
    nol+=1
    scribus.progressSet(nol)

    if nol >= len(black_cards):
        break # prevents adding a new page

    if count_row==colstotal and count_column==rowstotal:
        # starts new page
        scribus.newPage(-1)
        scribus.gotoPage(scribus.pageCount())
        count_row=1
        count_column=1
    elif count_row==colstotal:
        # starts new column
        count_row=1
        count_column+=1
    else:
        count_row+=1

scribus.messagebarText("Processed black cards.")
scribus.progressReset()
